package ru.t1consulting.nkolesnik.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    List<Task> findAll(@Nullable Sort sort);

    List<Task> findAll(@Nullable String userId, @Nullable Sort sort);

    List<Task> findAll(@Nullable Comparator comparator);

    List<Task> findAll(@Nullable String userId, @Nullable Comparator comparator);

    @NotNull
    List<Task> findAllByProjectId(@Nullable String projectId);

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    void removeByProjectId(@Nullable String projectId);

    void removeByProjectId(@Nullable String userId, @Nullable String projectId);

}
