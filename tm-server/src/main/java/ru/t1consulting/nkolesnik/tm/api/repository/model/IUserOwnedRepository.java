package ru.t1consulting.nkolesnik.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void add(@Nullable String userId, @Nullable M model);

    long getSize(@Nullable String userId);

    @NotNull
    List<M> findAll(@Nullable String userId);

    @Nullable
    M findById(@Nullable String userId, @Nullable String id);

    boolean existsById(@Nullable String userId, @Nullable String id);

    void clear(@Nullable String userId);

    void remove(@Nullable String userId, @Nullable M model);

    void removeById(@Nullable String userId, @Nullable String id);

}
