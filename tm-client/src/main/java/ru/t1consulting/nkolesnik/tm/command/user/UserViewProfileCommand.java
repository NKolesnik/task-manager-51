package ru.t1consulting.nkolesnik.tm.command.user;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserProfileRequest;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;

@Getter
public class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-view-profile";

    @NotNull
    public static final String DESCRIPTION = "Display user info.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showUser(getUserEndpoint().showProfileUser(new UserProfileRequest(getToken())).getUser());
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
